using System;
using System.Collections.Generic;
 
namespace GradeBook
{ 
    public delegate void GradeAddedDelegate(object sender, EventArgs args);

    public class NamedObect
    {
        public NamedObect(string name)
        {
            Name = name;
        }

        // crating a property (variable with setter and getter wihtout making the methods)
        public string Name
        {
            get;
            set;
        }
    }

    public interface IBook
    {
        void AddGrade(double grade);
        Statistics GetStatistics();
        string Name {get;}
        event GradeAddedDelegate GradeAdded;
    }
    public abstract class Book : NamedObect, IBook
    {
        protected Book(string name) : base(name)
        {
        }

        public abstract event GradeAddedDelegate GradeAdded;
        public abstract void AddGrade(double grade);

        public abstract Statistics GetStatistics();
    }

    public class InMemoryBook : Book
    {   
        public InMemoryBook(string name) : base(name)
        {
            grades = new List<double>();
            Name = name;
        }

        public void AddGrade(char letter)
        {
            switch(letter)
            {
                case 'A':
                    AddGrade(90);
                    break;
                case 'B':
                    AddGrade(80);
                    break;
                case 'C':
                    AddGrade(70);
                    break;
                case 'F':
                    AddGrade(60);
                    break;
                default:
                    AddGrade(0);
                    break;
            }
        }
        
        public override void AddGrade(double grade)
        {
            if  (grade <= 100 && grade >= 0)
            {
                this.grades.Add(grade);
                if(GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
            }
            else
            {
                throw new ArgumentException($"Invalid {nameof(grade)}");
            }
        }

        public override event GradeAddedDelegate GradeAdded;
 
        public override Statistics GetStatistics()
        {    
            var result = new Statistics();

             for (var i = 0; i < grades.Count; i++)
            {
                result.Add(grades[i]);    
            }

            return result;
        }

        private List<double> grades;
        public const string CATEGORY = "Science";
    } 
}
