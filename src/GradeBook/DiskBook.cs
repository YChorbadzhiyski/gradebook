using System;
using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    internal class DiskBook: Book
    {
        public DiskBook(string name) : base(name)
        {
            Name = name;
            path = name + ".txt";
        }

        public override event GradeAddedDelegate GradeAdded;

        public override void AddGrade(double grade)
        {
            if  (grade <= 100 && grade >= 0)
            {
            // same as to try/catch, try with resources
            using (var writer = File.AppendText(path))
            {
                writer.WriteLine(grade);
                if(GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
            }
            }else
            {
                throw new ArgumentException($"Invalid {nameof(grade)}");
            }
        }

        public override Statistics GetStatistics()
        {
            var result = new Statistics();
            using(var reader = File.OpenText(path))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    var number = double.Parse(line);
                    result.Add(number);
                    line = reader.ReadLine();
                }
            }

            return result;
        }
         string path = "";
    }
}