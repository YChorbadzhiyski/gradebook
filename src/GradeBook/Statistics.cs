using System;

namespace GradeBook
{
    public class Statistics
    {
        public Statistics()
        {   
            count = 0;
            Sum = 0.0;
            High = double.MinValue;
            Low = double.MaxValue;
        }
        public void Add(double number)
        {
            Sum += number;
            count += 1;
            High = Math.Max(High, number);
            Low = Math.Min(Low, number);
        }
        
        public int count;
        public double Sum;
        public double Average
        {
            get
            {
                return Sum/count;
            }
        }
        public double High;
        public double Low; 
        public char Letter
        {
            get
            {
                switch(Average)
                {
                    case var d when d >= 90.0:
                        return'A';
                    case var d when d >= 80:
                        return'B';     
                    case var d when d >= 70:
                        return'C';
                    case var d when d >= 60:
                        return'D';
                    default:
                        return'F';
                }
            }
        }
    }
}